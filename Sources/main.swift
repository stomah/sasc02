import Foundation

func error(_ message: String) -> Never {
	fputs(message + "\n", stderr)
	exit(1)
}


struct Location {
	let file: String
	var line: Int
	
	func error(_ message: String) -> Never {
		sasc02.error("\(file):\(line): \(message)")
	}
}


enum TokenKind: Equatable, Hashable {
	case eof
	case newline
	
	case keywordByte
	case keywordWord
	case keywordDword
	
	case keywordRes
	case keywordStruct
	case keywordGlobalstruct
	case keywordEnd
	case keywordSizeof
	
	case character(Character)
	case repeatedCharacter(Character)
	case id(Substring)
	case localLabel(Substring)
	case int(UInt32)
	case string(Substring)
}

struct Token {
	let location: Location
	let kind: TokenKind
	
	func expectId() -> Substring {
		if case .id(let id) = kind {
			return id
		} else {
			location.error("expected an identifier")
		}
	}
}

struct Lexer {
	private(set) var location: Location
	private(set) var content: String
	
	private var c: Character? { content.first }
	
	init(filePath: String) {
		location = Location(file: filePath, line: 1)
		do {
			content = try String(contentsOfFile: filePath)
		} catch {
			sasc02.error("failed to read file '\(filePath)'")
		}
	}
	
	private mutating func nextKind() -> TokenKind {
		func lexNumber(_ f: (Character) -> Bool, radix: Int) -> TokenKind {
			let num = content.prefix(while: f)
			content.removeFirst(num.count)
			if let n = UInt32(num, radix: radix) {
				return .int(n)
			} else {
				location.error("constant cannot fit in 32 bits")
			}
		}
		
		switch c {
		case nil:
			return .eof
		case "$":
			content.removeFirst()
			if !(c?.isHexDigit ?? false) {
				location.error("expected hex digits")
			}
			return lexNumber(\.isHexDigit, radix: 16)
		case "%":
			content.removeFirst()
			if c == "0" || c == "1" {
				return lexNumber({ $0 == "0" || $0 == "1" }, radix: 2)
			} else {
				return .character("%")
			}
		case "'":
			content.removeFirst()
			let n = content.removeFirst().unicodeScalars.first!.value
			if c != "'" {
				location.error("expected a closing quote after character literal")
			}
			content.removeFirst()
			return .int(n)
		case "\"":
			content.removeFirst()
			let string = content.prefix(while: { $0 != "\"" })
			content.removeFirst(string.count + 1)
			return .string(string)
		case ".":
			content.removeFirst()
			let keyword = content.prefix(while: { $0.isLetter || $0.isNumber || $0 == "_" })
			content.removeFirst(keyword.count)
			switch keyword {
			case "byte": return .keywordByte
			case "word": return .keywordWord
			case "dword": return .keywordDword
			case "res": return .keywordRes
			case "struct": return .keywordStruct
			case "globalstruct": return .keywordGlobalstruct
			case "end": return .keywordEnd
			case "sizeof": return .keywordSizeof
			default: location.error("keyword '\(keyword)' does not exist")
			}
		case "\n":
			content.removeFirst()
			location.line += 1
			return .newline
		case "<", ">", "+", "-", ":":
			let ch = c!
			content.removeFirst()
			if c == ch {
				content.removeFirst()
				return .repeatedCharacter(ch)
			} else {
				return .character(ch)
			}
		case "=", ",", "(", ")", "[", "]", "*", "/", "&", "~", "|":
			let ch = c!
			content.removeFirst()
			return .character(ch)
		case "@":
			content.removeFirst()
			let label = content.prefix(while: { $0.isLetter || $0.isNumber || $0 == "_" })
			content.removeFirst(label.count)
			return .localLabel(label)
		default:
			if let first = c {
				if first.isLetter || first == "_" {
					let id = content.prefix(while: { $0.isLetter || $0.isNumber || $0 == "_" || $0 == "." || $0 == "#" })
					content.removeFirst(id.count)
					return .id(id)
				}
				if first.isNumber {
					return lexNumber(\.isNumber, radix: 10)
				}
			}
			location.error("unrecognized character")
		}
	}
	
	mutating func next() -> Token {
		content.trimPrefix(while: { $0 == " " || $0 == "\t" })
		if c == ";" {
			content.trimPrefix(while: { !$0.isNewline })
			return next()
		}
		return Token(location: location, kind: nextKind())
	}
}



/*
file:
	{ item | '\n' } EOF

item:
	[ID | LOCAL_LABEL] ':'
	line-item '\n'

line-item:
	ID '=' expr
	ID [expr]
	'.byte' (expr | STRING) { ',' (expr | STRING) }
	('.word' | '.dword') expr { ',' expr }
	'.res' expr [',' expr]
	'.incbin' STRING
	'.struct' ID [expr] '\n' { [ID ['='] expr] '\n' } '.end'
	'.globalstruct' expr '\n' { [ID expr] '\n' } '.end'

expr:
	expr-or { ('+' | '-') expr-or }

expr-or:
	expr-xor { '|' expr-xor }

expr-xor:
	expr-and { '^' expr-and }

expr-and:
	expr-shift { '&' expr-shift }

expr-shift:
	expr-mul { ('<<' | '>>') expr-mul }

expr-mul:
	expr-primary { ('*' | '/' | '%') expr-primary }

expr-primary:
	INT
	ID
	LOCAL_LABEL
	'++' | '--'
	('<' | '>') expr-primary
	ID '::' ID
	'.sizeof' ID
	'(' expr ')'
*/

/*
addressing mode syntax:

immediate             : .#

absolute              : .a
absolute + X          : .ax
absolute + Y          : .ay
absolute + X indirect : .axi
absolute indirect     : .ai

zero page             : .z
zero page + X         : .zx
zero page + Y         : .zy
zero page + X indirect: .zxi
zero page indirect    : .zi
zero page indirect + Y: .ziy
*/


enum ExpressionSize {
	case byte, word, dword
}

enum BinaryOperator {
	case add, sub, mul, div, mod, and, xor, or, shl, shr
}

indirect enum ExpressionKind {
	case int(UInt32)
	case symbol(Substring)
	case lowByte(Expression)
	case highByte(Expression)
	case binary(BinaryOperator, Expression, Expression)
}

struct Expression {
	let kind: ExpressionKind
	let location: Location
}

enum InstructionOrData {
	case expression(Expression, ExpressionSize)
	case branchOffset(Expression, relativeTo: UInt16)
	case constant([UInt8])
}

struct StructDefinition {
	let fieldOffsets: [Substring : UInt32]
	let size: UInt32
}


func help() -> Never {
	error("usage: sasc02 -origin=<PC_ORIGIN_HEX> [-o=<OUTPUT_FILE>] <INPUT_FILE>+")
}

var pc: UInt16?
var inputFiles: [String] = []
var outputFile: String?

for argument in CommandLine.arguments.dropFirst() {
	func testArgument(_ a: String) -> Substring? {
		if argument.starts(with: a) {
			return argument.dropFirst(a.count)
		} else {
			return nil
		}
	}
	if let origin = testArgument("-origin=") {
		if pc != nil {
			help()
		}
		if let pc1 = UInt16(origin, radix: 16) {
			pc = pc1
		} else {
			error("invalid origin")
		}
	} else if let outputFile1 = testArgument("-o=") {
		if outputFile != nil {
			help()
		}
		outputFile = String(outputFile1)
	} else {
		inputFiles.append(argument)
	}
}

guard var pc, !inputFiles.isEmpty else { help() }

var structs: [Substring : StructDefinition] = [:]
var symbols: [Substring : UInt32] = [:]
var instructions: [InstructionOrData] = []
var lastGlobalLabel: Substring = ""
var nextUnnamedLabel = 0

func makeUnnamedLabel(index: Int) -> Substring {
	"unnamed label #\(index)"
}

for inputFile in inputFiles {
	var lexer = Lexer(filePath: inputFile)
	var token = lexer.next()
	
	func next() {
		token = lexer.next()
	}
	
	func expect(_ expected: TokenKind) {
		if token.kind == expected {
			next()
		} else {
			token.location.error("expected \(expected)")
		}
	}
	
	func expectId() -> Substring {
		if case .id(let id) = token.kind {
			next()
			return id
		} else {
			token.location.error("expected an identifier")
		}
	}
	
	func parsePrimaryExpression() -> Expression {
		let location = token.location
		let kind: ExpressionKind
		switch token.kind {
		case .character("("):
			next()
			let expression = parseExpression()
			expect(.character(")"))
			return expression
		case .int(let int):
			next()
			kind = .int(int)
		case .id(let id):
			next()
			if token.kind == .repeatedCharacter(":") {
				guard let structDefinition = structs[id] else {
					token.location.error("undefined struct \(id)")
				}
				next()
				let fieldName = expectId()
				guard let fieldOffset = structDefinition.fieldOffsets[fieldName] else {
					token.location.error("struct \(id) doesn't have field \(fieldName)")
				}
				kind = .int(UInt32(fieldOffset))
			} else {
				kind = .symbol(id)
			}
		case .localLabel(let l):
			next()
			kind = .symbol("\(lastGlobalLabel)@\(l)")
		case .repeatedCharacter("+"):
			next()
			kind = .symbol(makeUnnamedLabel(index: nextUnnamedLabel))
		case .repeatedCharacter("-"):
			next()
			kind = .symbol(makeUnnamedLabel(index: nextUnnamedLabel - 1))
		case .character("*"):
			next()
			kind = .int(UInt32(pc))
		case .keywordSizeof:
			next()
			let structTag = expectId()
			guard let structDefinition = structs[structTag] else {
				token.location.error("undefined struct \(structTag)")
			}
			kind = .int(UInt32(structDefinition.size))
		case .character("<"):
			next()
			kind = .lowByte(parseExpression())
		case .character(">"):
			next()
			kind = .highByte(parseExpression())
		default:
			token.location.error("expected an expression")
		}
		return .init(kind: kind, location: location)
	}
	
	func parseBinaryExpression(left: Expression, precedence: Int = 0) -> Expression {
		func getCurrentOperator() -> (Int, BinaryOperator)? {
			let precedences: [TokenKind : (Int, BinaryOperator)] = [
				.character("+")        : (10, .add),
				.character("-")        : (10, .sub),
				.character("|")        : (15, .or ),
				.character("~")        : (16, .xor),
				.character("&")        : (17, .and),
				.repeatedCharacter("<"): (18, .shl),
				.repeatedCharacter(">"): (18, .shr),
				.character("*")        : (20, .mul),
				.character("/")        : (20, .div),
				.character("%")        : (20, .mod),
			]
			return precedences[token.kind]
		}
		var left = left
		while true {
			guard let (tokenPrecedence, op) = getCurrentOperator() else {
				return left
			}
			if tokenPrecedence < precedence {
				return left
			}
			let location = token.location
			next()
			var right = parsePrimaryExpression()
			if tokenPrecedence < getCurrentOperator()?.0 ?? -1 {
				right = parseBinaryExpression(left: right, precedence: tokenPrecedence + 1)
			}
			left = .init(kind: .binary(op, left, right), location: location)
		}
	}
	
	func parseExpression() -> Expression {
		parseBinaryExpression(left: parsePrimaryExpression())
	}
	
	while true {
		while token.kind == .newline { next() }
		if token.kind == .eof { break }
		var wasLabel = false
		let idLocation = token.location
		switch token.kind {
		case .keywordByte:
			repeat {
				next()
				if case .string(let string) = token.kind {
					instructions.append(.constant(.init(string.utf8)))
					pc &+= UInt16(string.utf8.count)
					next()
				} else {
					instructions.append(.expression(parseExpression(), .byte))
					pc &+= 1
				}
			} while token.kind == .character(",")
		
		case .keywordWord:
			repeat {
				next()
				instructions.append(.expression(parseExpression(), .word))
				pc &+= 2
			} while token.kind == .character(",")
		
		case .keywordDword:
			repeat {
				next()
				instructions.append(.expression(parseExpression(), .dword))
				pc &+= 4
			} while token.kind == .character(",")
		
		case .keywordRes:
			next()
			let count = eval(parseExpression())
			var repeatedByte: UInt8 = 0
			if token.kind == .character(",") {
				next()
				let l = token.location
				let repeatedValue = eval(parseExpression())
				guard let repeatedByte1 = UInt8(exactly: repeatedValue) else {
					l.error("repeated value (\(repeatedValue)) must fit in a byte")
				}
				repeatedByte = repeatedByte1
			}
			instructions.append(.constant(.init(repeating: repeatedByte, count: Int(count))))
			pc &+= UInt16(count)
		
		case .keywordStruct:
			next()
			let structTag = expectId()
			let origin = token.kind == .newline ? 0 : eval(parseExpression())
			var offset = origin
			expect(.newline)
			var fieldOffsets: [Substring : UInt32] = [:]
			while true {
				while token.kind == .newline {
					next()
				}
				if case .id(let fieldName) = token.kind {
					next()
					if token.kind == .character("=") {
						next()
						fieldOffsets[fieldName] = eval(parseExpression())
					} else {
						if fieldOffsets[fieldName] != nil {
							token.location.error("redefining field '\(fieldName)'")
						}
						fieldOffsets[fieldName] = offset
						offset += eval(parseExpression())
					}
					expect(.newline)
				} else {
					break
				}
			}
			expect(.keywordEnd)
			structs[structTag] = .init(fieldOffsets: fieldOffsets, size: offset - origin)
		
		case .keywordGlobalstruct:
			next()
			var offset = eval(parseExpression())
			expect(.newline)
			while true {
				while token.kind == .newline {
					next()
				}
				if case .id(let fieldName) = token.kind {
					if symbols[fieldName] != nil {
						token.location.error("redefining symbol '\(fieldName)'")
					}
					next()
					let fieldSize = eval(parseExpression())
					expect(.newline)
					symbols[fieldName] = offset
					offset += fieldSize
				} else {
					break
				}
			}
			expect(.keywordEnd)
		
		case .localLabel(let localLabel):
			let name = lastGlobalLabel + "@" + localLabel
			if symbols[name] != nil {
				token.location.error("redefining local label '\(localLabel)'")
			}
			symbols[name] = UInt32(pc)
			next()
			expect(.character(":"))
			wasLabel = true
		
		case .character(":"):
			symbols[makeUnnamedLabel(index: nextUnnamedLabel)] = UInt32(pc)
			nextUnnamedLabel += 1
			next()
			wasLabel = true
		
		case .id(let id):
			next()
			if token.kind == .character(":") {
				if symbols[id] != nil {
					token.location.error("redefining symbol '\(id)'")
				}
				symbols[id] = UInt32(pc)
				lastGlobalLabel = id
				next()
				wasLabel = true
			} else if token.kind == .character("=") {
				if symbols[id] != nil {
					token.location.error("redefining symbol '\(id)'")
				}
				next()
				symbols[id] = eval(parseExpression())
			} else {
				guard let (opcode, operandType) = mapping[id] else {
					idLocation.error("instruction '\(id)' does not exist")
				}
				instructions.append(.constant([opcode]))
				switch operandType {
				case .none:
					break
				case .byte:
					instructions.append(.expression(parseExpression(), .byte))
				case .word:
					instructions.append(.expression(parseExpression(), .word))
				case .branchOffset:
					instructions.append(.branchOffset(parseExpression(), relativeTo: pc &+ 2))
				}
				pc &+= UInt16(operandType.instructionSize)
			}
		
		default:
			token.location.error("expected line content")
		}
		if !wasLabel {
			expect(.newline)
		}
	}
}



func eval(_ expression: Expression) -> UInt32 {
	switch expression.kind {
	case let .int(int):
		return int
	case let .symbol(name):
		if let symbol = symbols[name] {
			return symbol
		} else {
			expression.location.error("undefined symbol '\(name)'")
		}
	case let .lowByte(expr):
		return eval(expr) & 0xFF
	case let .highByte(expr):
		return (eval(expr) >> 8) & 0xFF
	case let .binary(operator_, left, right):
		let l = eval(left)
		let r = eval(right)
		if r == 0 && (operator_ == .div || operator_ == .mod) {
			expression.location.error("division by zero")
		}
		switch operator_ {
		case .add: return l &+ r
		case .sub: return l &- r
		case .mul: return l &* r
		case .div: return l / r
		case .mod: return l % r
		case .and: return l & r
		case .xor: return l ^ r
		case .or : return l | r
		case .shl: return l << r
		case .shr: return l >> r
		}
	}
}

var code: [UInt8] = []
for instruction in instructions {
	switch instruction {
	case let .constant(data):
		code.append(contentsOf: data)
	case let .expression(expression, size):
		let value = eval(expression)
		switch size {
		case .byte:
			if let v = UInt8(exactly: value) {
				code.append(v)
			} else {
				expression.location.error("value $\(String(value, radix: 16, uppercase: true)) cannot fit in 8 bits")
			}
		case .word:
			if let v = UInt16(exactly: value) {
				code.append(UInt8(truncatingIfNeeded: v))
				code.append(UInt8(value >> 8))
			} else {
				expression.location.error("value $\(String(value, radix: 16, uppercase: true)) cannot fit in 16 bits")
			}
		case .dword:
			code.append(UInt8(truncatingIfNeeded: value))
			code.append(UInt8(truncatingIfNeeded: value >> 8))
			code.append(UInt8(truncatingIfNeeded: value >> 16))
			code.append(UInt8(value >> 24))
		}
	case let .branchOffset(expression, relativeTo: o):
		let offset = Int(eval(expression)) - Int(o)
		if let offset8 = Int8(exactly: offset) {
			code.append(UInt8(bitPattern: offset8))
		} else {
			expression.location.error("branch offset (\(offset)) does not fit in a signed 8-bit integer")
		}
	}
}

if let outputFile {
	if !FileManager.default.createFile(atPath: outputFile, contents: Data(code)) {
		error("failed to write output file")
	}
} else {
	print(code)
}
