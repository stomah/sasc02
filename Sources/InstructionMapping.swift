/*let mapping: [Substring : [OperandKind? : UInt8]] = [
	"lda": [
		.immediate: 0xA9,
		.absolute(nil): 0xAD,
		.absolute(.x): 0xBD,
		.absolute(.y): 0xB9,
		.zeroPage(nil): 0xA5,
		.zeroPage(.x): 0xB5,
		.zeroPage(.xi): 0xA1,
		.zeroPage(.i): 0xB2,
		.zeroPage(.iy): 0xB1,
	],
	"ldx": [
		.immediate: 0xA2,
		.absolute(nil): 0xAE,
		.absolute(.y): 0xBE,
		.zeroPage(nil): 0xA6,
		.zeroPage(.y): 0xB6,
	],
	"ldy": [
		.immediate: 0xA0,
		.absolute(nil): 0xAC,
		.absolute(.x): 0xBC,
		.zeroPage(nil): 0xA4,
		.zeroPage(.x): 0xB4,
	],
	"ina": [ nil: 0x1A ],
	"rts": [ nil: 0x60 ],
	"sta": [
		.absolute(nil): 0x8D,
		.absolute(.x): 0x9D,
		.absolute(.y): 0x99,
		.zeroPage(nil): 0x85,
		.zeroPage(.xi): 0x81,
		.zeroPage(.x): 0x95,
		.zeroPage(.i): 0x92,
		.zeroPage(.iy): 0x91,
	],
	"stx": [
		.absolute(nil): 0x8E,
		.zeroPage(nil): 0x86,
		.zeroPage(.y): 0x96,
	],
	"sty": [
		.absolute(nil): 0x8C,
		.zeroPage(nil): 0x84,
		.zeroPage(.x): 0x94,
	],
	"stz": [
		.absolute(nil): 0x9C,
		.absolute(.x): 0x9E,
		.zeroPage(nil): 0x64,
		.zeroPage(.x): 0x74,
	],
	"stp": [ nil: 0xDB ],
]*/

enum InstructionOperandType {
	case none, byte, word
	case branchOffset   // operand is evaluated as a word but translated to a branch offset
	
	var instructionSize: UInt8 {
		switch self {
		case .none: return 1
		case .byte, .branchOffset: return 2
		case .word: return 3
		}
	}
}

let mapping: [Substring : (opcode: UInt8, InstructionOperandType)] = [
	"bra": (0x80, .branchOffset),
	
	"bzc": (0xD0, .branchOffset),
	"bzs": (0xF0, .branchOffset),
	"bne": (0xD0, .branchOffset),
	"beq": (0xF0, .branchOffset),
	"bnz": (0xD0, .branchOffset),
	"bz" : (0xF0, .branchOffset),
	
	"bcc": (0x90, .branchOffset),
	"bcs": (0xB0, .branchOffset),
	"blt": (0x90, .branchOffset),
	"bge": (0xB0, .branchOffset),
	
	"bpl": (0x10, .branchOffset),
	"bmi": (0x30, .branchOffset),
	"bnc": (0x10, .branchOffset),
	"bns": (0x30, .branchOffset),
	
	"bvc": (0x50, .branchOffset),
	"bvs": (0x70, .branchOffset),
	
	"jmp"    : (0x4C, .word),
	"jmp.axi": (0x7C, .word),
	"jmp.ai" : (0x6C, .word),
	
	"jsr": (0x20, .word),
	
	"adc.a"  : (0x6D, .word),
	"adc.ax" : (0x7D, .word),
	"adc.ay" : (0x79, .word),
	"adc.#"  : (0x69, .byte),
	"adc.z"  : (0x65, .byte),
	"adc.zxi": (0x61, .byte),
	"adc.zx" : (0x75, .byte),
	"adc.zi" : (0x72, .byte),
	"adc.ziy": (0x71, .byte),
	
	"sbc.a"  : (0xED, .word),
	"sbc.ax" : (0xFD, .word),
	"sbc.ay" : (0xF9, .word),
	"sbc.#"  : (0xE9, .byte),
	"sbc.z"  : (0xE5, .byte),
	"sbc.zxi": (0xE1, .byte),
	"sbc.zx" : (0xF5, .byte),
	"sbc.zi" : (0xF2, .byte),
	"sbc.ziy": (0xF1, .byte),
	
	"and.a"  : (0x2D, .word),
	"and.ax" : (0x3D, .word),
	"and.ay" : (0x39, .word),
	"and.#"  : (0x29, .byte),
	"and.z"  : (0x25, .byte),
	"and.zxi": (0x21, .byte),
	"and.zx" : (0x35, .byte),
	"and.zi" : (0x32, .byte),
	"and.ziy": (0x31, .byte),
	
	"xor.a"  : (0x4D, .word),
	"xor.ax" : (0x5D, .word),
	"xor.ay" : (0x59, .word),
	"xor.#"  : (0x49, .byte),
	"xor.z"  : (0x45, .byte),
	"xor.zxi": (0x41, .byte),
	"xor.zx" : (0x55, .byte),
	"xor.zi" : (0x52, .byte),
	"xor.ziy": (0x51, .byte),
	
	"or.a"  : (0x0D, .word),
	"or.ax" : (0x1D, .word),
	"or.ay" : (0x19, .word),
	"or.#"  : (0x09, .byte),
	"or.z"  : (0x05, .byte),
	"or.zxi": (0x01, .byte),
	"or.zx" : (0x15, .byte),
	"or.zi" : (0x12, .byte),
	"or.ziy": (0x11, .byte),
	
	"cpa.a"  : (0xCD, .word),
	"cpa.ax" : (0xDD, .word),
	"cpa.ay" : (0xD9, .word),
	"cpa.#"  : (0xC9, .byte),
	"cpa.z"  : (0xC5, .byte),
	"cpa.zxi": (0xC1, .byte),
	"cpa.zx" : (0xD5, .byte),
	"cpa.zi" : (0xD2, .byte),
	"cpa.ziy": (0xD1, .byte),
	
	"lda.a"  : (0xAD, .word),
	"lda.ax" : (0xBD, .word),
	"lda.ay" : (0xB9, .word),
	"lda.#"  : (0xA9, .byte),
	"lda.z"  : (0xA5, .byte),
	"lda.zxi": (0xA1, .byte),
	"lda.zx" : (0xB5, .byte),
	"lda.zi" : (0xB2, .byte),
	"lda.ziy": (0xB1, .byte),
	
	"sta.a"  : (0x8D, .word),
	"sta.ax" : (0x9D, .word),
	"sta.ay" : (0x99, .word),
	"sta.z"  : (0x85, .byte),
	"sta.zxi": (0x81, .byte),
	"sta.zx" : (0x95, .byte),
	"sta.zi" : (0x92, .byte),
	"sta.ziy": (0x91, .byte),
	
	
	"shl.a" : (0x0E, .word),
	"shl.ax": (0x1E, .word),
	"shl"   : (0x0A, .none),
	"shl.z" : (0x06, .byte),
	"shl.zx": (0x16, .byte),
	
	"shr.a" : (0x4E, .word),
	"shr.ax": (0x5E, .word),
	"shr"   : (0x4A, .none),
	"shr.z" : (0x46, .byte),
	"shr.zx": (0x56, .byte),
	
	"rol.a" : (0x2E, .word),
	"rol.ax": (0x3E, .word),
	"rol"   : (0x2A, .none),
	"rol.z" : (0x26, .byte),
	"rol.zx": (0x36, .byte),
	
	"ror.a" : (0x6E, .word),
	"ror.ax": (0x6E, .word),
	"ror"   : (0x6A, .none),
	"ror.z" : (0x66, .byte),
	"ror.zx": (0x76, .byte),
	
	"bit.a" : (0x2C, .word),
	"bit.ax": (0x3C, .word),
	"bit.#" : (0x89, .byte),
	"bit.z" : (0x24, .byte),
	"bit.zx": (0x34, .byte),
	
	"ldx.a" : (0xAE, .word),
	"ldx.ay": (0xBE, .word),
	"ldx.#" : (0xA2, .byte),
	"ldx.z" : (0xA6, .byte),
	"ldx.zy": (0xB6, .byte),
	
	"ldy.a" : (0xAC, .word),
	"ldy.ax": (0xBC, .word),
	"ldy.#" : (0xA0, .byte),
	"ldy.z" : (0xA4, .byte),
	"ldy.zx": (0xB4, .byte),
	
	"stx.a" : (0x8E, .word),
	"stx.z" : (0x86, .byte),
	"stx.zy": (0x96, .byte),
	
	"sty.a" : (0x8C, .word),
	"sty.z" : (0x84, .byte),
	"sty.zx": (0x94, .byte),
	
	"stz.a" : (0x9C, .word),
	"stz.ax": (0x9E, .word),
	"stz.z" : (0x64, .byte),
	"stz.zx": (0x74, .byte),
	
	"cpx.a": (0xEC, .word),
	"cpx.#": (0xE0, .byte),
	"cpx.z": (0xE4, .byte),
	
	"cpy.a": (0xCC, .word),
	"cpy.#": (0xC0, .byte),
	"cpy.z": (0xC4, .byte),
	
	"inc.a" : (0xEE, .word),
	"inc.ax": (0xFE, .word),
	"inc.z" : (0xE6, .byte),
	"inc.zx": (0xF6, .byte),
	
	"dec.a" : (0xCE, .word),
	"dec.ax": (0xDE, .word),
	"dec.z" : (0xC6, .byte),
	"dec.zx": (0xD6, .byte),
	
	"tcb.a": (0x1C, .word),
	"tcb.z": (0x14, .byte),
	
	"tsb.a": (0x0C, .word),
	"tsb.z": (0x04, .byte),
	
	
	"ina": (0x1A, .none),
	"inx": (0xE8, .none),
	"iny": (0xC8, .none),
	
	"dea": (0x3A, .none),
	"dex": (0xCA, .none),
	"dey": (0x88, .none),
	
	"pha": (0x48, .none),
	"phx": (0xDA, .none),
	"phy": (0x5A, .none),
	"php": (0x08, .none),
	
	"pla": (0x68, .none),
	"plx": (0xFA, .none),
	"ply": (0x7A, .none),
	"plp": (0x28, .none),
	
	"tax": (0xAA, .none),
	"tay": (0xA8, .none),
	"txa": (0x8A, .none),
	"tya": (0x98, .none),
	
	"tsx": (0xBA, .none),
	"txs": (0x9A, .none),
	
	"clc": (0x18, .none),
	"cld": (0xD8, .none),
	"cli": (0x58, .none),
	"clv": (0xB8, .none),
	
	"sec": (0x38, .none),
	"sed": (0xF8, .none),
	"sei": (0x78, .none),
	
	"rts": (0x60, .none),
	"rti": (0x40, .none),
	"nop": (0xEA, .none),
	"brk": (0x00, .byte),
	"wai": (0xCB, .none),
	"stp": (0xDB, .none),
]
